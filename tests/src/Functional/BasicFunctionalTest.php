<?php

declare(strict_types=1);

namespace Drupal\Tests\vault_key_aws\Functional;

use Drupal\Tests\BrowserTestBase;
use PHPUnit\Framework\Attributes\Group;

/**
 * Test description.
 */
#[Group('vault_key_aws')]
final class BasicFunctionalTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['vault_key_aws'];

  /**
   * Test callback.
   */
  public function testModuleInstalls(): void {
    $module_handler = $this->container->get('module_handler');
    $this->assertTrue($module_handler->moduleExists('vault_key_aws'));
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
  }

}
