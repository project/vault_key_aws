<?php

namespace Drupal\Tests\vault_key_aws\Unit;

use Drupal\Core\Form\FormState;
use Drupal\key\KeyInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\vault\VaultClientInterface;
use Drupal\vault_key_aws\Plugin\KeyProvider\VaultAWSKeyProvider;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Vault\Exceptions\RequestException;
use Vault\ResponseModels\Response;

/**
 * Tests the Vault KV Value Key Provider plugin.
 *
 * @group vault_key_aws
 *
 * @covers \Drupal\vault_key_aws\Plugin\KeyProvider\VaultAWSKeyProvider
 * @codeCoverageIgnore
 */
class VaultAwsKeyProviderTest extends UnitTestCase {

  /**
   * VaultClint mock.
   *
   * @var \Drupal\vault\VaultClientInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected VaultClientInterface|MockObject $vaultClientMock;

  /**
   * LoggerInterface mock.
   *
   * @var \Psr\Log\LoggerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $loggerMock;

  /**
   * The VaultKeyValueKeyProvider plugin.
   *
   * @var \Drupal\vault_key_aws\Plugin\KeyProvider\VaultAWSKeyProvider
   */
  protected VaultAWSKeyProvider $plugin;

  /**
   * Default plugin configuration.
   *
   * @var array
   */
  protected array $configuration;

  /**
   * List of mount points returned by listSecretEngineMounts mock.
   *
   * @var string[]
   */
  protected array $mountValues;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->configuration = [
      'secret_engine_mount' => 'aws/',
      'secret_path' => '',
    ];

    $this->vaultClientMock = $this->createMock(VaultClientInterface::class);
    $this->mountValues = [
      'secret/',
      'some_other_secret_mount/',
    ];
    $this->vaultClientMock
      ->method('listSecretEngineMounts')
      ->willReturn($this->mountValues);

    $this->loggerMock = $this->createMock(LoggerInterface::class);

    $settings = [
      'vault.settings' => [
        'base_url' => 'http://vault:8200',
      ],
    ];

    /** @var \Drupal\Core\Config\ConfigFactoryInterface|\PHPUnit\Framework\MockObject\MockObject $config_factory_stub */
    $config_factory_stub = $this->getConfigFactoryStub($settings);
    $this->plugin = new VaultAwsKeyProvider($this->configuration, 'vault_aws', [], $this->vaultClientMock, $this->loggerMock, $this->getStringTranslationStub(), $config_factory_stub);

  }

  /**
   * Validate the plugin was constructed during setUp().
   */
  public function testValidatePluginInstance(): void {
    $this->assertInstanceOf(VaultAWSKeyProvider::class, $this->plugin, 'Plugin is a VaultKeyValueKeyProvider');
  }

  /**
   * Test the plugin create() method.
   */
  public function testCreate(): void {

    $settings = [
      'vault.settings' => [
        'base_url' => 'http://vault:8200',
      ],
    ];

    /** @var \Drupal\Core\Config\ConfigFactoryInterface|\PHPUnit\Framework\MockObject\MockObject $config_factory_stub */
    $config_factory_stub = $this->getConfigFactoryStub($settings);

    $service_map = [
      [
        'vault.vault_client',
        ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE,
        $this->vaultClientMock,
      ],
      [
        'logger.channel.vault',
        ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE,
        $this->loggerMock,
      ],
      [
        'string_translation',
        ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE,
        $this->getStringTranslationStub(),
      ],
      [
        'config.factory',
        ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE,
        $config_factory_stub,
      ],
    ];

    $container_mock = $this->createMock(ContainerInterface::class);
    $container_mock->method('get')
      ->willReturnMap($service_map);

    $plugin = VaultAWSKeyProvider::create($container_mock, $this->configuration, 'vault_aws', []);
    $this->assertInstanceOf(VaultAWSKeyProvider::class, $plugin, 'Create returns AppRole plugin');
  }

  /**
   * Test obtain Default Configuration.
   */
  public function testDefaultConfiguration(): void {
    $this->assertIsArray($this->plugin->defaultConfiguration(), 'Default Configuration returns array');
  }

  /**
   * Test obtain key values.
   */
  public function testGetKeyValue(): void {

    $expected_secret = '{"access_key":"AKIAZF76T4JGL3SMABON","secret_key":"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","security_token":null}';

    $retrieve_lease_map = [
      ['key:test_key', ''],
      ['key:cached_key', $expected_secret],
    ];

    $this->vaultClientMock->method('retrieveLease')
      ->willReturnMap($retrieve_lease_map);

    $this->vaultClientMock->method('read')
      ->willReturnCallback([$this, 'getKeyValueResponse']);

    $this->vaultClientMock
      ->expects($this->exactly(1))
      ->method('storeLease')
      ->with(
        $this->stringStartsWith('key:test_key'),
        $this->equalTo('aws/creds/test_aws_role/8764y4FvJNupAVNJdM7QvrJq'),
        $this->equalTo($expected_secret),
        $this->equalTo(2764800),
        $this->equalTo(TRUE)
      );

    $key_mock = $this->createMock(KeyInterface::class);
    $key_mock->method('id')->willReturn('test_key');
    $cached_key_mock = $this->createMock(KeyInterface::class);
    $cached_key_mock->method('id')->willReturn('cached_key');

    $this->assertEquals($expected_secret, $this->plugin->getKeyValue($cached_key_mock), 'Cached lease returns correctly');
    $this->assertEquals($expected_secret, $this->plugin->getKeyValue($key_mock), 'Successful test returned correct data');
    $this->assertEmpty($this->plugin->getKeyValue($key_mock), 'Request exception returned empty string');
    $this->assertEquals($expected_secret, $this->plugin->getKeyValue($key_mock), 'no lease duration still returns data');
  }

  /**
   * Provides mock data for Vault calls in getKeyValue().
   *
   * @param string $path
   *   Ignored path.
   *
   * @return \Vault\ResponseModels\Response
   *   Mock Vault response.
   *
   * @throws \Vault\Exceptions\RequestException
   *   Exception on mock request error.
   */
  public function getKeyValueResponse(string $path): Response {

    $response_data = [
      'requestId' => '882b0428-60df-0318-5325-3ae8b4c35622',
      'auth' => NULL,
      'data' => [
        'access_key' => 'AKIAZF76T4JGL3SMABON',
        'secret_key' => '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',
        'security_token' => NULL,
      ],
      'leaseId' => 'aws/creds/test_aws_role/8764y4FvJNupAVNJdM7QvrJq',
      'leaseDuration' => 2764800,
      'renewable' => TRUE,
    ];

    static $counter = 0;
    $counter++;

    switch ($counter) {
      case 1:
        return new Response($response_data);

      case 2;
        throw new RequestException();

      case 3:
        unset($response_data['leaseId']);
        unset($response_data['leaseDuration']);
        return new Response($response_data);

      default:
        throw new \Exception('unexpected call to getKeyValueResponse');
    }
  }

  /**
   * Test set key values.
   */
  public function testSetKeyValue(): void {

    $key_mock = $this->createMock(KeyInterface::class);
    $key_mock->method('id')->willReturn('test_key');

    $this->assertFalse($this->plugin->setKeyValue($key_mock, 'data'), 'setKeyValue should always return false');
  }

  /**
   * Test delete key values.
   */
  public function testDeleteKeyValue(): void {

    $this->vaultClientMock->method('revokeLease')
      ->with('key:test_key')
      ->willReturnOnConsecutiveCalls(TRUE, FALSE);

    $key_mock = $this->createMock(KeyInterface::class);
    $key_mock->method('id')->willReturn('test_key');

    $this->assertTrue($this->plugin->deleteKeyValue($key_mock), 'Delete key value returned TRUE on success');
    $this->assertFalse($this->plugin->deleteKeyValue($key_mock), 'Delete key value returned FALSE on request exception');
  }

  /**
   * Provides mock data for Vault calls in setKeyValue().
   *
   * @param string $path
   *   Ignored path.
   *
   * @return \Vault\ResponseModels\Response
   *   Mock Vault response.
   *
   * @throws \Vault\Exceptions\RequestException
   *   Exception on mock request error.
   */
  public function deleteKeyValueResponseProvider(string $path): Response {

    static $counter = 0;
    $counter++;

    switch ($counter) {
      case 1:
        return new Response(['data' => ['data' => ['value' => 'my_value']]]);

      case 2;
        throw new RequestException();

      default:
        throw new \Exception('unexpected call to getKeyValueResponse');
    }
  }

  /**
   * Test build configuration form.
   */
  public function testBuildConfigurationForm(): void {
    $form_state = new FormState();
    $this->assertIsArray($this->plugin->buildConfigurationForm([], $form_state), 'Form builder returns an array for form');
  }

  /**
   * Test build configuration form.
   *
   * @dataProvider providerValidateConfigForm
   */
  public function testValidateConfigurationForm(string $path_prefix, int $expected_error_count): void {
    $form_state = new FormState();
    $form = $this->plugin->buildConfigurationForm([], $form_state);
    $form_state->setValue('secret_path', $path_prefix);
    $this->plugin->validateConfigurationForm($form, $form_state);
    $this->assertCount($expected_error_count, $form_state->getErrors(), "Expected error count matched");
  }

  /**
   * Provide data for tetValidateConfigurationForm().
   */
  public static function providerValidateConfigForm(): array {
    return [
      'Single folder' => [
        'test',
        0,
      ],
      'Under multiple folders' => [
        'test/test1/test',
        0,
      ],
      'Contains invalid characters' => [
        'contains/invalid/ | /characters',
        1,
      ],
    ];
  }

  /**
   * Test submitConfigurationForm.
   */
  public function testSubmitConfigurationForm(): void {
    $form_state = new FormState();
    $form = $this->plugin->buildConfigurationForm([], $form_state);
    $values = [
      'secret_path' => 'test1/test2',
    ];
    $form_state->setValues($values);
    $this->plugin->submitConfigurationForm($form, $form_state);
    $this->assertEquals($values, $this->plugin->getConfiguration(), 'Configuration returns as set');
  }

  /**
   * Validate that the key value is obscured.
   */
  public function testObscureKeyValue(): void {

    $secret = '{"access_key":"AKIAZF76T4JGL3SMABON","secret_key":"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","security_token":null}';
    $expected_obscured_secret = '{"access_key":"****************ABON","secret_key":"************************************v7Zu","security_token":""}';
    $this->assertEquals($expected_obscured_secret, $this->plugin->obscureKeyValue($secret, ['key_type_group' => 'authentication_multivalue']));
  }

}
