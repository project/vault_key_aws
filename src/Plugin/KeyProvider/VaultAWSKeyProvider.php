<?php

namespace Drupal\vault_key_aws\Plugin\KeyProvider;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Serialization\Json;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\vault\VaultClient;
use Drupal\vault\VaultClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Psr\Log\LoggerInterface;
use Drupal\key\KeyInterface;
use Drupal\key\Plugin\KeyPluginFormInterface;
use Drupal\key\Plugin\KeyProviderBase;
use Drupal\key\Plugin\KeyProviderSettableValueInterface;

/**
 * Adds a key provider that fetches AWS credentials from HashiCorp Vault.
 *
 * @KeyProvider(
 *   id = "vault_aws",
 *   label = "Vault AWS",
 *   description = @Translation("This provider fetches AWS credentials from the HashiCorp Vault AWS secret engine."),
 *   storage_method = "vault_aws",
 *   key_value = {
 *     "accepted" = FALSE,
 *     "required" = FALSE
 *   }
 * )
 */
final class VaultAWSKeyProvider extends KeyProviderBase implements KeyProviderSettableValueInterface, KeyPluginFormInterface {

  /**
   * The settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $settings;

  /**
   * The Vault client.
   *
   * @var \Drupal\vault\VaultClientInterface
   */
  protected VaultClientInterface $client;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * Vault service config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $vaultConfig;

  /**
   * Constructs a VaultAwsKeyProvider object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\vault\VaultClientInterface $vault_client
   *   A Drupal Vault module client.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   String translation service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config_factory service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, VaultClientInterface $vault_client, LoggerInterface $logger, TranslationInterface $string_translation, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setStringTranslation($string_translation);
    $this->setClient($vault_client);
    $this->setLogger($logger);

    $this->vaultConfig = $config_factory->get('vault.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('vault.vault_client'),
      $container->get('logger.channel.vault'),
      $container->get('string_translation'),
      $container->get('config.factory')
    );
  }

  /**
   * Sets client property.
   *
   * @param \Drupal\vault\VaultClientInterface $client
   *   The secrets manager client.
   *
   * @return $this
   *   Current object.
   */
  public function setClient(VaultClientInterface $client): static {
    $this->client = $client;
    return $this;
  }

  /**
   * Sets logger property.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   *
   * @return $this
   *   Current object.
   */
  public function setLogger(LoggerInterface $logger): static {
    $this->logger = $logger;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'secret_engine_mount' => 'aws/',
      'secret_path' => '',
    ];
  }

  /**
   * Returns the lease storage key for this provider.
   *
   * @param \Drupal\key\KeyInterface $key
   *   The key entity.
   *
   * @return string
   *   The storage key.
   */
  protected static function leaseStorageKey(KeyInterface $key): string {
    return sprintf("key:%s", $key->id());
  }

  /**
   * {@inheritdoc}
   */
  public function getKeyValue(KeyInterface $key): string {
    // Check if there is a valid lease available.
    $lease_data = $this->client->retrieveLease(self::leaseStorageKey($key));
    if (!empty($lease_data) && is_string($lease_data)) {
      return $lease_data;
    }

    $this->logger->debug("no valid lease - reading new credentials for " . $key->id());
    $path = $this->buildRequestPath("get", $key);
    try {
      $response = $this->client->read($path);
      $data = Json::encode($response->getData());
      if ($response->getLeaseId() !== NULL && $response->getLeaseDuration() !== NULL) {
        $this->client->storeLease(
          self::leaseStorageKey($key),
          $response->getLeaseId(),
          $data,
          $response->getLeaseDuration(),
          $response->isRenewable() ?? FALSE,
        );
      }
      return $data;
    }
    catch (\Exception $e) {
      $this->logger->critical('Unable to fetch secret ' . $key->id());
      return '';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setKeyValue(KeyInterface $key, $key_value): bool {
    // There's nothing to do here - we only support reading aws credentials.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteKeyValue(KeyInterface $key): bool {
    // Revoke the lease.
    $this->logger->debug("key entity deletion triggered revocation of aws credential lease");
    return $this->client->revokeLease(self::leaseStorageKey($key));
  }

  /**
   * {@inheritdoc}
   */
  public static function obscureKeyValue($key_value, array $options = []): string {
    switch ($options['key_type_group']) {
      case 'authentication_multivalue':
        // Obscure the values of each element of the object to make it more
        // clear what the contents are.
        $options['visible_right'] = 4;

        $json = Json::decode($key_value);
        if (!is_array($json)) {
          throw new \Exception('Unable to decode provided key_value');
        }
        foreach ($json as $key => $value) {
          $json[$key] = self::obscureValue($value, $options);
        }
        $obscured_value = Json::encode($json);
        break;

      default:
        $obscured_value = parent::obscureKeyValue($key_value, $options);
    }

    return $obscured_value;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $provider_config = $this->getConfiguration();
    $new = empty($form_state->getStorage()['key_value']['current']);

    $form['secret_engine_mount'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret Engine Mount'),
      '#description' => $this->t('The Key/Value secret engine mount point.'),
      '#field_prefix' => sprintf('%s/%s/', $this->vaultConfig->get('base_url'), VaultClient::API),
      '#default_value' => $provider_config['secret_engine_mount'],
      '#disabled' => !$new,
    ];

    try {
      // Attempt to provide better UX by listing the available mounts. If this
      // fails it will fall back to the standard textfied input.
      $mount_points = $this->client->listSecretEngineMounts(['aws']);

      $form['secret_engine_mount']['#type'] = 'select';
      $form['secret_engine_mount']['#options'] = [];
      foreach ($mount_points as $mount => $info) {
        $form['secret_engine_mount']['#options'][$mount] = $mount;
      }
    }
    catch (\Exception $e) {
      $this->logger->error("Unable to list mount points for key/value secret engine");
    }

    $form['secret_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Role Path'),
      '#description' => $this->t('The vault role path.'),
      '#default_value' => $provider_config['secret_path'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $key_provider_settings = $form_state->getValues();

    // Ensure secret path is only url safe characters.
    if (preg_match('/[^a-z_\-\/0-9]/i', $key_provider_settings['secret_path'])) {
      $form_state->setErrorByName('secret_path', $this->t('Secret Path Prefix only supports the following characters: a-z 0-9 . - _ /'));
      return;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->setConfiguration($form_state->getValues());
  }

  /**
   * Builds the URL endpoint.
   *
   * @param string $action
   *   The action being performed.
   * @param \Drupal\key\KeyInterface $key
   *   The key entity.
   *
   * @return string
   *   Request path for desired vault endpoint.
   */
  protected function buildRequestPath(string $action, KeyInterface $key): string {
    $provider_config = $this->getConfiguration();

    switch ($action) {
      case 'get':
        $url = new FormattableMarkup("/:secret_engine_mount:endpoint/:secret_path", [
          ':secret_engine_mount' => $provider_config['secret_engine_mount'],
          ':endpoint' => 'creds',
          ':secret_path' => $provider_config['secret_path'],
        ]);
        return (string) $url;

      default:
        return '';
    }

  }

}
