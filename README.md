CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers
* Previous Maintainers

INTRODUCTION
------------

This module provides support for using the Key module API to retrieve a secret
from the AWS Secret Engine.

* For the full description of the module visit:
  https://www.drupal.org/project/vault_key_aws

* To submit bug reports and feature suggestions, or to track changes visit:
  https://www.drupal.org/project/issues/vault_key_aws

REQUIREMENTS
------------

* [Vault](https://www.drupal.org/project/issues/vault)
* [Key](https://www.drupal.org/project/key)

INSTALLATION
------------

Install the Vault Key AWS module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.

CONFIGURATION
-------------

Please refer to the Key Module documentation for details on configuring keys.

1. To use this module select a Key Provider of "Vault AWS"
2. Select the path from the secret engine dropdown that refers to you AWS
   Secrets Engine.
3. Provide the remaining path details to reference the role in the Vault Server.

MAINTAINERS
-----------

* Conrad Lara - https://www.drupal.org/u/cmlara

PREVIOUS MAINTAINERS
--------------------
Nick Santamaria - https://www.drupal.org/u/nicksanta
